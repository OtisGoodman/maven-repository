**What is this**
This is a public repo which anyone can use as a Maven repository. This repo hosts the deobfuscated lotr mod and the deobfuscated Ainulindalë.

**How to use**
Simply add the repository to your `build.gradle` and add the wanted repositories. The repository url is `https://gitlab.com/eras-of-arda/maven-repository/-/raw/master/repo/`.

**Example**
```
repositories {
    maven {
        name = "EoA Repository"
        url = "https://gitlab.com/eras-of-arda/maven-repository/-/raw/master/repo/"
    }
}

dependencies {
    compileOnly "lotr:lotr:v36.4"
}
```